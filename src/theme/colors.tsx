"use strict"

export const lightGreenColors = {
    background: '#36AD50',
    background2: "#43D663",
    background3: "#fff",
    primary: '#226E32',
    secondary: '#4AED6D',
    text: '#000',
    text2: '#353535',
    error: '#D32F2F',
    link: '##19339e'
};

export const darkColors = {
    background: '#000',
    background2: '#121212',
    background3: '#181820',
    primary: '#f37121',
    secondary: '#c70039',
    text: '#FFFFFF',
    text2: '#8A8A8A',
    error: '#EF9A9A',
    link: '#19929e'
};

export const darkOrangeColors = {
    background: '#000',
    background2: '#121212',
    background3: '#181820',
    primary: '#f37121',
    secondary: '#c70039',
    text: '#FFFFFF',
    text2: '#8A8A8A',
    error: '#EF9A9A',
    link: '#19929e'
};

export const darkPinkColors = {
    ...darkColors,
    primary: "#a45aef",
    secondary: "#58159b"
}

export const darkGreenColors = {
    ...darkColors,
    primary: "#359137",
    secondary: "#046806"
}